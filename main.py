#!/usr/bin/env python3

import mmh3
import argparse
import numpy as np
from numpy.linalg import norm


class Bloom():

    def __init__(self, num_vectors, num_dimensions, num_hashes=2):
        self._num_vectors = num_vectors
        self._num_dimensions = num_dimensions
        self._num_hashed = num_hashes
        for v in range(num_vectors):
            for d in range(num_dimensions):
                self._table[v].append(0)

    def set_vector(self, word, vector):
        for seed in range(self._num_hashes):
            hash = mmh3.hash(word, seed=seed)
            row = hash % self._num_vectors
            self._table[row] = vector / self._num_hashes

    def get_vector(self, word):
        retVal = 0
        for seed in range(self._num_hashes):
            hash = mmh3.hash(word, seed=seed)
            row = hash % self._num_hashes
            retVal += self._table[row]
        return retVal

    def update_vector(self, word, vector):
        retVal = 0
        for seed in range(self._num_hashes):
            hash = mmh3.hash(word, seed=seed)
            row = hash % self._num_hashes
            self._table[row] -= 0.001 * vector
            retVal += self._table[row]
        return retVal


def encode(word):
    encoding = {}
    encoding['position'] = []
    encoding['frequency'] = []

    # Create empty vectors with a dimension for each ascii characters
    for i in range(256):
        encoding['position'].append(0)
        encoding['frequency'].append(0)

    # Populate vectors
    for i, c in enumerate(word):
        pos = ord(c)
        val = encoding['position'][pos] + i + 1
        encoding['position'][pos] = val
        encoding['frequency'][pos] += 1

    # Normalize the vectors
    for i in range(256):
        encoding['position'][i] /= ((len(word)/2)*(1+len(word)))
        encoding['frequency'][i] /= len(word)

    # Merge the normalized vectors together
    final_encoding = encoding['position'] + encoding['frequency']
    return final_encoding


def cosine_similarity(first, second):
    first_np = np.array(first)
    second_np = np.array(second)
    return np.dot(first_np, second_np) / (norm(first_np)*norm(second_np))


def main(args):
    tolerance = args.tolerance
    # Encode search term
    term_encode = encode(args.term.lower())

    # Open words file
    file = open("/usr/share/dict/words", 'r')
    lines = file.readlines()

    max_val = 0.0
    retVal = {}
    # Search for similar term
    for line in lines:
        line_encode = encode(line.strip().lower())

        # Get the cosine similarity between the two
        similarity = cosine_similarity(term_encode, line_encode)

        # Find closest word
        if similarity >= max_val:
            max_val = max(max_val, similarity)
            retVal[line.strip()] = {}
            retVal[line.strip()]['cosine'] = similarity
            retVal[line.strip()]['encode'] = line_encode

    for key, val in retVal.items():
        if val['cosine'] >= max_val + tolerance:
            print(key, val['cosine'])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='Search',
                                     description='Experiment to search a dictionary for a word')
    parser.add_argument('term')
    parser.add_argument('-t',
                        '--tolerance',
                        action='store',
                        default=0,
                        type=int)

    main(parser.parse_args())

# TODO: Create HashEmbedding for each vector
#         Create vector
#         SHA256 Hash the value
#         Save dict of vector and hash
# TODO: Run through dictionary first and populate Vocab with all values
# TODO: Check if search word is in HashEmbedding before finding closest match
